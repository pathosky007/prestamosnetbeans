package modelo.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import modelo.Region;

public class RegionDAO extends DB implements DAO{

	public int create(Region region) {
		int resultado = 0;
		Connection con = this.getConnection();
		
		String consulta = "INSERT INTO Region VALUES(?)";
		
		if(!(con ==null)) {
			try {
				PreparedStatement stmt = con.prepareStatement(consulta);
				stmt.setString(1, region.getNombre());
				resultado = stmt.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return resultado;
	}
	
	@Override
	public int update(int id, String campo, String valor) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object get(int catidad) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}

