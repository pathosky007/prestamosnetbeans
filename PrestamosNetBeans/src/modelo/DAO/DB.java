    package modelo.DAO;
    
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
//import java.sql.Statement;

public class DB {
	private static final String Url = "jdbc:sqlserver://"
			+ "52.54.212.137:1433;"
			+ "databaseName=abiezerSifontesPrestamos;"
			+ "user=abiezer;"
			+ "password=1234";
	
	public Connection getConnection(){
		Connection con = null;
		try {
			con = DriverManager.getConnection(Url);
			return con;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return con;
	}

}
